package com.dantonio.luthiery.instrument.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.dantonio.luthiery.helper.bo.ClientBO;
import com.dantonio.luthiery.helper.system.AbstractMessage;
import com.dantonio.luthiery.helper.system.Globals;
import com.dantonio.luthiery.instrument.model.Instrument;
import com.dantonio.luthiery.instrument.model.repository.InstrumentRepository;
import com.dantonio.luthiery.instrument.service.InstrumentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;


@RefreshScope
@Service
public class InstrumentServiceImpl implements InstrumentService{

	private Logger log = LoggerFactory.getLogger(InstrumentService.class);
	
	private String GET_CLIENT_ADDRESS = "http://%1$s:%2$s/client/find/%3$s";
	
	@Value("${service.client.service}")
    private String clientServiceId; 
	
	@Autowired
	private EurekaClient eurekaClient;
	
	@Autowired
	private InstrumentRepository instrumentRepository;
	
	@Autowired
    private RestTemplate restTemplate;	
	
	@Override
	public Long createInstrument(Instrument instrument) {
		try{			
			Instrument newInst = instrumentRepository.save(instrument);
			return newInst.getIdClient();
		} catch ( Exception ex ) {
			log.error("ERRO ON SAVE NEW INSTRUMENT");			
		}
		return null;
	}

	@Override
	public Collection<Instrument> findAllInstruments() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Instrument> findInstrumentById(Long id) throws Exception{
		try {
			return instrumentRepository.findById(id);
		}catch (Exception e) {
			log.error("ERROR FINDING INSTRUMENT ID:" + id);
			log.error(e.getMessage());
			throw new Exception();
		}
	}

	@Override
	public Optional<ClientBO> getClient(Long idClient) throws Exception{		
		try {
			Application eurekApp = eurekaClient.getApplication(clientServiceId);
			InstanceInfo instanceInfo = eurekApp.getInstances().get(0);
			
			String url = String.format(
					GET_CLIENT_ADDRESS, 
					instanceInfo.getIPAddr(),
					instanceInfo.getPort(), 
					idClient);

			AbstractMessage result = restTemplate.getForObject(url, AbstractMessage.class);
			ClientBO client = null;
			if(result.getResult().equalsIgnoreCase(Globals.SUCCESS)) {
				client  = new ObjectMapper().readValue(result.getObjReturn(), ClientBO.class);
			} else {
				client =  new ClientBO();
			}
			return Optional.of(client);			
		} catch (Exception ex) {
			log.error("ERRO AO BUSCAR CLIENTE: " + ex.getClass().getName(), ex.getMessage());
			ex.printStackTrace();
			throw new Exception();
		}
	}

	@Override
	public Collection<Instrument> findInstrumentsByTypeId(Long instrumentId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<Instrument> findInstrumentsByClientId(Long idClient) {
		List<Instrument> instruments = new ArrayList<>();		
		try {
			instruments = instrumentRepository.findInstrumentsByIdClient(idClient);
		} catch (Exception ex) {
			log.error("ERROR ON FIND INSTRUMENTS BY CLIENT. CLIENT ID: " + idClient);
			log.error(ex.getMessage());
			log.error(ex.getCause().toString());			
		}
		return instruments;
	}	
	
}
