package com.dantonio.luthiery.instrument.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dantonio.luthiery.instrument.model.Type;
import com.dantonio.luthiery.instrument.model.repository.TypeRepository;
import com.dantonio.luthiery.instrument.service.TypeService;

@Service
public class TypeServiceImpl implements TypeService{

	private static final Logger log = LoggerFactory.getLogger(TypeServiceImpl.class);
	
	@Autowired
	TypeRepository typeRepository;
	
	@Override
	public Long createType(Type type) throws Exception {
		try {
			Type typeAdded = typeRepository.save(type);
			return typeAdded.getIdType();
		} catch (Exception ex) {
			log.error("ERRO ON CREATING NEW TYPE: " + ex.getStackTrace());
			return Long.valueOf(0);
		}		
	}

	@Override
	public List<Type> findAllTypes() throws Exception {
		return typeRepository.findAll();
	}

	@Override
	public Optional<Type> findTypeById(Long id) throws Exception {
		try {
			return typeRepository.findById(id);		
		} catch(Exception ex) {
			log.error("ERROR ON FINDING TYPE BY ID:" + ex.getStackTrace());
			log.error(ex.getCause().toString());
			throw new Exception();
		}
	}
	
	@Override
	public boolean updateType(Type type) throws Exception {
		try {
			typeRepository.save(type);
			return true;
		} catch(Exception ex) {
			log.error("ERROR ON EDITING TYPE:" + ex.getStackTrace());
			log.error(ex.getCause().toString());
			throw new Exception();			
		}
	}

	@Override
	public boolean deleteType(Long typeId) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean existTypeById(Long id) throws Exception {
		try {
			return typeRepository.existsById(id);			
		} catch(Exception ex) {
			log.error("ERROR ON FINDING TYPE BY ID");
			log.error(ex.getMessage());
			log.error(ex.getCause().toString());
			throw new Exception();
		}
	}
}
