package com.dantonio.luthiery.instrument.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dantonio.luthiery.helper.controller.AbstractController;
import com.dantonio.luthiery.helper.system.ErrorMessage;
import com.dantonio.luthiery.instrument.model.Model;
import com.dantonio.luthiery.instrument.service.ModelService;


@Controller
@RequestMapping(path="/instrument-model")
public class ModelController extends AbstractController {

	private static final Logger log = LoggerFactory.getLogger(ModelController.class);
	
	@Autowired 
	ModelService modelService;
	
	@Autowired
	MessageSource messageSource;
	
	@PostMapping(value="/add", consumes = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody ResponseEntity addNewModel(@Valid @RequestBody Model model, BindingResult bindingResult) {
		ErrorMessage error = new ErrorMessage();
		try {			
			if(bindingResult.hasErrors()) {							
				error = addError(400, 
						messageSource.getMessage(
								"standard.client.messages.insert.failed", null, LocaleContextHolder.getLocale()),
								bindingResult);
			} else {
				if(modelService.createModel(model) != 0) {
					return ResponseEntity.ok(this.addSuccess(201, messageSource.getMessage("standard.client.messages.insert.success", new String[] {Model.class.getSimpleName()}, LocaleContextHolder.getLocale())));
				}
			}
		} catch (Exception ex) {
			log.error(messageSource.getMessage("standard.log.message.insert.failed", new String[] {Model.class.getSimpleName()}, LocaleContextHolder.getLocale()) + ": " + ex.getMessage());
			ex.printStackTrace();			
			error = addError(417,
							messageSource.getMessage("standard.messages.unexpected.error", null, LocaleContextHolder.getLocale()),
							ex.getMessage()
					);
		}
		return ResponseEntity.badRequest().body(error);
	}
}
