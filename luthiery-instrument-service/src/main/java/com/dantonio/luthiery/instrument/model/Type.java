package com.dantonio.luthiery.instrument.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.LazyCollection;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Type {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_type")
	private Long idType;

	@NotNull(message="Instrument type name cannot be null")
	@Size(min=2, message="Instrument type name should have at least 2 characters")
	private String name;
	
	@JsonIgnoreProperties({"type", "brand", "model"})
	@OneToMany(mappedBy = "type", fetch = FetchType.LAZY)
	private Set<Instrument> instruments;

	public Type() { }
	
	public Type(String name) {
		this.name = name;
	}
	
	public Long getIdType() {
		return idType;
	}

	public void setIdType(Long idType) {
		this.idType = idType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Instrument> getInstruments() {
		return instruments;
	}

	public void setInstruments(Set<Instrument> instruments) {
		this.instruments = instruments;
	}
	
	
}
