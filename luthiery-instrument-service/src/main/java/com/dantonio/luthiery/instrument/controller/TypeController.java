package com.dantonio.luthiery.instrument.controller;

import java.util.List;
import java.util.Set;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dantonio.luthiery.helper.controller.AbstractController;
import com.dantonio.luthiery.helper.system.AbstractMessage;
import com.dantonio.luthiery.helper.system.ErrorMessage;
import com.dantonio.luthiery.instrument.model.Instrument;
import com.dantonio.luthiery.instrument.model.Type;
import com.dantonio.luthiery.instrument.service.TypeService;

@Controller
@RequestMapping(path="/type")
public class TypeController extends AbstractController {
	
	private static final Logger log = LoggerFactory.getLogger(TypeController.class);
	
	@Autowired
	private TypeService typeService;
	
	@Autowired
	private MessageSource messageSource;
	
	@PostMapping(value="/add", consumes = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody ResponseEntity<AbstractMessage> addNewType(@Valid @RequestBody Type type, BindingResult bindingResult) {
		ErrorMessage error = new ErrorMessage();
		try {			
			if(bindingResult.hasErrors()) {
				error = addError(400, 
						messageSource.getMessage(
								"standard.client.messages.insert.failed", null, LocaleContextHolder.getLocale()),
								bindingResult);
			} else {
				log.debug("type:" + type);
				if(typeService.createType(type) != 0) {
					return ResponseEntity.ok(this.addSuccess(201, messageSource.getMessage("standard.client.messages.insert.success", new String[] {Type.class.getSimpleName()}, LocaleContextHolder.getLocale()))); 
				}
			}
		} catch (Exception ex) {			
			log.error(messageSource.getMessage("standard.log.message.insert.failed", new String[] {Type.class.getSimpleName()}, LocaleContextHolder.getLocale()) + ": " + ex.getMessage());
			ex.printStackTrace();			
			error = addError(417,
							messageSource.getMessage("standard.messages.unexpected.error", null, LocaleContextHolder.getLocale()),
							ex.getMessage()
					);			
		}
		return ResponseEntity.badRequest().body(error);
	}
	
	@PutMapping(value="/edit", consumes = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody ResponseEntity<AbstractMessage> editType(@Valid @RequestBody Type type, BindingResult bindingResult) {
		ErrorMessage error = new ErrorMessage();
		try {		
			if(type.getIdType() != null && typeService.existTypeById(type.getIdType())) {
				if(bindingResult.hasErrors()) {
					error = addError(400, 
							messageSource.getMessage(
									"standard.client.messages.update.failed", null, LocaleContextHolder.getLocale()),
									bindingResult);
				} else {				
					if(typeService.updateType(type)) {
						return ResponseEntity.ok(addSuccess(201, messageSource.getMessage("standard.client.messages.update.success", new String[] {Type.class.getSimpleName()}, LocaleContextHolder.getLocale()))); 
					}
				}
			} else {
				error = addError(400, 
						messageSource.getMessage("standard.client.message.notfound", new String[] {Type.class.getSimpleName()}, LocaleContextHolder.getLocale()));
			}			
		} catch (Exception ex) {			
			log.error(messageSource.getMessage("standard.log.message.update.failed", new String[] {Type.class.getSimpleName()}, LocaleContextHolder.getLocale()) + ": " + ex.getMessage());
			ex.printStackTrace();			
			error = addError(417,
							messageSource.getMessage("standard.messages.unexpected.error", null, LocaleContextHolder.getLocale()),
							ex.getMessage()
					);			
		}
		return ResponseEntity.badRequest().body(error);
	}	
	
	@GetMapping(path="/edit/{id}")
	public @ResponseBody ResponseEntity<AbstractMessage> findTypeById(@PathVariable Long id) {
		ErrorMessage error = addError(400, 
				messageSource.getMessage("standard.client.message.notfound.by.id", new String[] {Type.class.getSimpleName(), id.toString()}, LocaleContextHolder.getLocale()));
		try {
			Optional<Type> type = typeService.findTypeById(id);
			if(type.isPresent()) {
				return ResponseEntity.ok(addSuccess(200, "", type.get()));
			} 
		} catch(Exception ex) {
			log.error("ERROR ON FINDING TYPE BY ID:" + ex.getMessage());
			log.error(ex.getCause().toString());
			error.setCause(ex.getMessage() + ":" + ex.getCause().toString());
		}
		return ResponseEntity.badRequest().body(error);
	}
	
	@GetMapping(path="/list-all")
	public @ResponseBody ResponseEntity<AbstractMessage> listAll(){
		ErrorMessage error = addError(400, messageSource.getMessage("standard.client.message.notfound", new String[] {Type.class.getSimpleName()}, LocaleContextHolder.getLocale()));
		try {
			List<Type> types = typeService.findAllTypes();		
			if(!types.isEmpty()) {
				types.stream().forEach(it-> it.setInstruments(null));
				return ResponseEntity.ok(addSuccess(200, "", types));
			}
		} catch (Exception e) {
			log.error("ERROR ON LISTING ALL TYPES");
			log.error(e.getCause().toString());
			error.setCause(e.getMessage() + ": " + e.getCause().toString());
		}	
		return ResponseEntity.badRequest().body(error);		
	}
	
	@GetMapping(path="/list-all-complete")
	public @ResponseBody ResponseEntity<AbstractMessage> listAllComplete(){
		ErrorMessage error = addError(400, messageSource.getMessage("standard.client.message.notfound", new String[] {Type.class.getSimpleName()}, LocaleContextHolder.getLocale()));
		try {
			List<Type> types = typeService.findAllTypes();
			
			if(!types.isEmpty()) {
				return ResponseEntity.ok(addSuccess(200, "", types));				
			}			
		} catch (Exception e) {
			log.error("ERROR ON LISTING ALL TYPES");
			log.error(e.getCause().toString());
			error.setCause(e.getMessage() + ": " + e.getCause().toString());
		}	
		return ResponseEntity.badRequest().body(error);		
	}
}
