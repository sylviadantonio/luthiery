package com.dantonio.luthiery.instrument.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.dantonio.luthiery.helper.bo.ClientBO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Instrument {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_instrument")
	private Long idInstrumento;
	
	@NotNull(message="Instrument color cannot be null.")
	@Size(min=2, message="Instrument color should have at least 2 characters.")
	private String color;
	
	@ManyToOne	
	@NotNull(message="Instrument model cannot be null.")
	@JoinColumn(name="id_model")
	@JsonIgnoreProperties("instruments")
	private Model model;
	
	@ManyToOne	
	@NotNull(message="Instrument type cannot be null.")
	@JoinColumn(name="id_type")
	@JsonIgnoreProperties("instruments")
	private Type type;
	
	@NotNull(message="Instrument number of strings cannot be null.")
	@Size(min=1, message="Number of strings should have at least 1 character.")
	@Min(value=3, message="No instruments has less than 3 strings.")
	@Column(name="n_strings")
	private String numberOfStrings;
	
	@NotNull(message="Instrument client id cannot be null")
	@Min(value=1, message="Client id cannot be smaller than 1.")
	@Column(name="id_client")
	private Long idClient;
	
	private String uuid;
	
	@Column(name="dt_creation")
	private Date dateCreation;
	
	@Transient
	private ClientBO client;
	
	public Long getIdInstrumento() {
		return idInstrumento;
	}
	public void setIdInstrumento(Long idInstrumento) {
		this.idInstrumento = idInstrumento;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public Model getModel() {
		return model;
	}
	public void setModel(Model model) {
		this.model = model;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	
	public String getNumberOfStrings() {
		return numberOfStrings;
	}
	public void setNumberOfStrings(String numberOfStrings) {
		this.numberOfStrings = numberOfStrings;
	}
	public Long getIdClient() {
		return idClient;
	}
	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}
	public ClientBO getClient() {
		return client;
	}
	public void setClient(ClientBO client) {
		this.client = client;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
		
}
