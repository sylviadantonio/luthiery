package com.dantonio.luthiery.instrument.service;

import java.util.Collection;
import java.util.Optional;

import com.dantonio.luthiery.instrument.model.Brand;

public interface BrandService {

	public Long createBrand(Brand brand) throws Exception;
	
	public boolean updateBrand(Brand brand) throws Exception;
	
	public boolean deleteBrand(Long idBrand) throws Exception;
	
	public Collection<Brand> findAllBrands() throws Exception;
	
	public Optional<Brand> findBrandById(Long idBrand) throws Exception;
	
	public boolean existBrandById(Long id) throws Exception;
}
