package com.dantonio.luthiery.instrument.service.impl;

import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dantonio.luthiery.instrument.model.Brand;
import com.dantonio.luthiery.instrument.model.repository.BrandRepository;
import com.dantonio.luthiery.instrument.model.repository.TypeRepository;
import com.dantonio.luthiery.instrument.service.BrandService;

@Service
public class BrandServiceImpl implements BrandService{
	
	private static final Logger log = LoggerFactory.getLogger(BrandServiceImpl.class);
	
	@Autowired
	private BrandRepository brandRepository;

	@Override
	public Long createBrand(Brand brand) throws Exception {
		try {
			Brand brandAdded = brandRepository.save(brand);
			return brandAdded.getIdBrand();
		} catch (Exception ex) {
			log.error("ERRO ON CREATING NEW BRAND: " + ex.getStackTrace());
			return Long.valueOf(0);
		}	
	}

	@Override
	public boolean updateBrand(Brand brand) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteBrand(Long idBrand) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Collection<Brand> findAllBrands() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Brand> findBrandById(Long idBrand) throws Exception {
		try {
			return brandRepository.findById(idBrand);		
		} catch(Exception ex) {
			log.error("ERROR ON FINDING BRAND BY ID:" + ex.getStackTrace());
			log.error(ex.getCause().toString());
			throw new Exception();
		}
	}

	@Override
	public boolean existBrandById(Long id) throws Exception {
		try {
			return brandRepository.existsById(id);			
		} catch(Exception ex) {
			log.error("ERROR ON FINDING BRAND BY ID");
			log.error(ex.getMessage());
			log.error(ex.getCause().toString());
			return false;
		}
	}

}
