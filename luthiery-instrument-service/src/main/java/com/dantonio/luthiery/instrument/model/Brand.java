package com.dantonio.luthiery.instrument.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Brand {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_brand")
	private Long idBrand;
	
	@NotNull(message="Instrument brand name cannot be null")
	@Size(min=2, message="Instrument brand name should have at least 2 characters")
	private String name;
	
	@OneToMany
	@JoinColumn(name="id_brand")
	@JsonIgnoreProperties({"instruments", "brand"})	
	private Set<Model> models;

	public Long getIdBrand() {
		return idBrand;
	}

	public void setIdBrand(Long idBrand) {
		this.idBrand = idBrand;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Model> getModels() {
		return models;
	}

	public void setModels(Set<Model> models) {
		this.models = models;
	}	
	
}
