package com.dantonio.luthiery.instrument.model.repository;

import org.springframework.data.repository.CrudRepository;

import com.dantonio.luthiery.instrument.model.Model;

public interface ModelRepository extends CrudRepository<Model, Long> {

}
