package com.dantonio.luthiery.instrument.service.impl;

import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dantonio.luthiery.instrument.model.Model;
import com.dantonio.luthiery.instrument.model.repository.ModelRepository;
import com.dantonio.luthiery.instrument.service.ModelService;

@Service
public class ModelServiceImpl implements ModelService{

	private static final Logger log = LoggerFactory.getLogger(ModelServiceImpl.class);
	
	@Autowired
	ModelRepository modelRepository;
	
	@Override
	public Long createModel(Model model) throws Exception {
		try {
			Model modelAdded = modelRepository.save(model);
			return modelAdded.getIdModel();
		} catch(Exception ex) {
			log.error("ERRO ON CREATING NEW MODEL: " + ex.getMessage());
			return Long.valueOf(0);
		}
	}

	@Override
	public boolean updateModel(Model model) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteModel(Long modelId) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Collection<Model> findAllModels() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Model> findModelById() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
