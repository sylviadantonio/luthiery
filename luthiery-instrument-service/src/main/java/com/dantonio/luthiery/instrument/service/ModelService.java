package com.dantonio.luthiery.instrument.service;

import java.util.Collection;
import java.util.Optional;

import com.dantonio.luthiery.instrument.model.Model;

public interface ModelService {

	public Long createModel(Model model) throws Exception;
	
	public boolean updateModel(Model model) throws Exception;
	
	public boolean deleteModel(Long modelId) throws Exception;
	
	public Collection<Model>findAllModels() throws Exception;
	
	public Optional<Model> findModelById() throws Exception;
	
}
