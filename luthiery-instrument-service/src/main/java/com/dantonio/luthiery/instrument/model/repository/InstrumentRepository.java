package com.dantonio.luthiery.instrument.model.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.dantonio.luthiery.instrument.model.Instrument;

public interface InstrumentRepository extends CrudRepository<Instrument, Long>{

	public List<Instrument> findInstrumentsByIdClient(Long idClient); 

}
