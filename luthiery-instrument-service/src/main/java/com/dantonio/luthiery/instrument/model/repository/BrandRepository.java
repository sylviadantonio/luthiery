package com.dantonio.luthiery.instrument.model.repository;

import org.springframework.data.repository.CrudRepository;

import com.dantonio.luthiery.instrument.model.Brand;

public interface BrandRepository extends CrudRepository<Brand, Long> {

}
