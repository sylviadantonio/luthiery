package com.dantonio.luthiery.instrument.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.dantonio.luthiery.instrument.model.Type;

public interface TypeRepository extends JpaRepository<Type, Long>{

}
