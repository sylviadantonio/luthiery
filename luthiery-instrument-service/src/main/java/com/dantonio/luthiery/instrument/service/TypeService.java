package com.dantonio.luthiery.instrument.service;

import java.util.List;
import java.util.Optional;

import com.dantonio.luthiery.instrument.model.Type;


public interface TypeService {

	public Long createType(Type type) throws Exception;
	
	public boolean updateType(Type type) throws Exception;
	
	public boolean deleteType(Long typeId) throws Exception;
	
	public List<Type> findAllTypes() throws Exception;
	
	public Optional<Type> findTypeById(Long id) throws Exception;
	
	public boolean existTypeById(Long id) throws Exception;
}
