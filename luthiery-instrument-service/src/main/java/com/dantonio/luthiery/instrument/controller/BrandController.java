package com.dantonio.luthiery.instrument.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dantonio.luthiery.helper.controller.AbstractController;
import com.dantonio.luthiery.helper.system.AbstractMessage;
import com.dantonio.luthiery.helper.system.ErrorMessage;
import com.dantonio.luthiery.instrument.model.Brand;
import com.dantonio.luthiery.instrument.service.BrandService;

@Controller
@RequestMapping(path="/brand")
public class BrandController extends AbstractController{
	
	private static final Logger log = LoggerFactory.getLogger(BrandController.class);

	@Autowired
	private BrandService brandService;
	
	@Autowired
	MessageSource messageSource;
	
	@PostMapping(value="/add", consumes= {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody ResponseEntity<AbstractMessage> addNewBrand(@Valid @RequestBody Brand brand, BindingResult bindingResult) {
		ErrorMessage error = new ErrorMessage();
		try {			
			if(bindingResult.hasErrors()) {							
				error = addError(400, 
						messageSource.getMessage(
								"standard.client.messages.insert.failed", null, LocaleContextHolder.getLocale()),
								bindingResult);
			} else {
				if(brandService.createBrand(brand) != 0) {
					return ResponseEntity.ok(addSuccess(201, messageSource.getMessage("standard.client.messages.insert.success", new String[] {Brand.class.getSimpleName()}, LocaleContextHolder.getLocale())));
				}
			}
		} catch (Exception ex) {
			log.error(messageSource.getMessage("standard.log.message.insert.failed", new String[] {Brand.class.getSimpleName()}, LocaleContextHolder.getLocale()) + ": " + ex.getMessage());
			ex.printStackTrace();			
			error = addError(417,
							messageSource.getMessage("standard.messages.unexpected.error", null, LocaleContextHolder.getLocale()),
							ex.getMessage()
					);
		}
		return ResponseEntity.badRequest().body(error);
	}
	
	
	@PutMapping(value="/edit", consumes = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody ResponseEntity<AbstractMessage> editBrand(@Valid @RequestBody Brand brand, BindingResult bindingResult) {
		ErrorMessage error = new ErrorMessage();
		try {		
			if(brand.getIdBrand() != null && brandService.existBrandById(brand.getIdBrand())) {
				if(bindingResult.hasErrors()) {
					error = addError(400, 
							messageSource.getMessage(
									"standard.client.messages.update.failed", null, LocaleContextHolder.getLocale()),
									bindingResult);
				} else {				
					if(brandService.updateBrand(brand)) {
						return ResponseEntity.ok(addSuccess(201, messageSource.getMessage("standard.client.messages.update.success", new String[] {Brand.class.getSimpleName()}, LocaleContextHolder.getLocale()))); 
					}
				}
			} else {
				error = addError(400, 
						messageSource.getMessage("standard.client.message.notfound", new String[] {Brand.class.getSimpleName()}, LocaleContextHolder.getLocale()));
			}			
		} catch (Exception ex) {			
			log.error(messageSource.getMessage("standard.log.message.update.failed", new String[] {Brand.class.getSimpleName()}, LocaleContextHolder.getLocale()) + ": " + ex.getMessage());
			ex.printStackTrace();			
			error = addError(417,
							messageSource.getMessage("standard.messages.unexpected.error", null, LocaleContextHolder.getLocale()),
							ex.getMessage()
					);			
		}
		return ResponseEntity.badRequest().body(error);
	}	
	
	@GetMapping(path="/edit/{id}")
	public @ResponseBody ResponseEntity<AbstractMessage> findBrandById(@PathVariable Long id) {
		ErrorMessage error = addError(400, 
				messageSource.getMessage("standard.client.message.notfound.by.id", new String[] {Brand.class.getSimpleName(), id.toString()}, LocaleContextHolder.getLocale()));
		try {
			Optional<Brand> brand = brandService.findBrandById(id);
			if(brand.isPresent()) {
				return ResponseEntity.ok(addSuccess(200, "", brand.get()));
			} 
		} catch(Exception ex) {
			log.error("ERROR ON FINDING BRAND BY ID:" + ex.getMessage());
			log.error(ex.getCause().toString());
			error.setCause(ex.getMessage() + ":" + ex.getCause().toString());
		}
		return ResponseEntity.badRequest().body(error);
	}
	
}
