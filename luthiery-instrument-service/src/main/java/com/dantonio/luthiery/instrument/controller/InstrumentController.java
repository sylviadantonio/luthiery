package com.dantonio.luthiery.instrument.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dantonio.luthiery.helper.bo.ClientBO;
import com.dantonio.luthiery.helper.controller.AbstractController;
import com.dantonio.luthiery.helper.system.AbstractMessage;
import com.dantonio.luthiery.helper.system.ErrorMessage;
import com.dantonio.luthiery.helper.system.Globals;
import com.dantonio.luthiery.instrument.model.Instrument;
import com.dantonio.luthiery.instrument.service.InstrumentService;


@Controller
@RequestMapping(path="/instrument")
public class InstrumentController extends AbstractController{

	private Logger log = LoggerFactory.getLogger(InstrumentController.class);
	
	@Autowired
	private InstrumentService instrumentService;	
	
	@Autowired
	MessageSource messageSource;
	
	@PostMapping(value="/add", consumes = {MediaType.APPLICATION_JSON_VALUE})	
	public @ResponseBody ResponseEntity addNewInstrument(@Valid @RequestBody Instrument instrument, BindingResult bindingResult) {
		ErrorMessage error = new ErrorMessage();		
		try {			
			if(bindingResult.hasErrors()) {							
				error = addError(400, 
						messageSource.getMessage(
								"standard.client.messages.insert.failed", null, LocaleContextHolder.getLocale()),
								bindingResult);
			} else {
				instrument.setUuid(UUID.randomUUID().toString());
				instrument.setDateCreation(new Date());
				if(instrumentService.createInstrument(instrument) != null) {
					return ResponseEntity.ok(this.addSuccess(201, messageSource.getMessage("standard.client.messages.insert.success", new String[] {Instrument.class.getSimpleName()}, LocaleContextHolder.getLocale())));
				}
			}
		} catch (Exception ex) {
			log.error(messageSource.getMessage("standard.log.message.insert.failed", new String[] {Instrument.class.getSimpleName()}, LocaleContextHolder.getLocale()) + ": " + ex.getMessage());
			ex.printStackTrace();			
			error = addError(417,
							messageSource.getMessage("standard.messages.unexpected.error", null, LocaleContextHolder.getLocale()),
							ex.getMessage()
					);
		}
		return ResponseEntity.badRequest().body(error);
	}
	
	@GetMapping(path="/list-all")
	public @ResponseBody Iterable<Instrument> getAllInstruments() {
		try {
			return instrumentService.findAllInstruments();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		}
		return null; 
	}
	
	
	@GetMapping(path="/edit/{id}")
	public @ResponseBody HashMap<String, Instrument> findInstrumentById(@PathVariable Long id) {
		HashMap<String, Instrument> result = new HashMap<>();
		try {
			Optional<Instrument> instrument = instrumentService.findInstrumentById(id);
			if(instrument.isPresent()) {
				Instrument inst = instrument.get(); 
				Optional<ClientBO> client = instrumentService.getClient(inst.getIdClient());
				
				if(client.isPresent()){
					log.info("encontrou cliente");
					log.info(client.get().getName());
					inst.setClient(client.get());					
				} else {
					log.info("NAO encontrou cliente");
					inst.setClient(new ClientBO());					
				}
				result.put("success", inst);
			}
		} catch(Exception ex) {
			log.error("ERROR: " + ex.getClass() + ex.getMessage());
			result.put("There was an error on return the instrument", new Instrument());
		}
		return result;
	}
	
	@GetMapping(path="/find-instrument-by-client-id/{id}")
	public @ResponseBody ResponseEntity<AbstractMessage> findInstrumentByClientId(@PathVariable Long id) {
		ErrorMessage error = new ErrorMessage();
		
		try {
			List<Instrument> instruments = instrumentService.findInstrumentsByClientId(id);
			if(!instruments.isEmpty()) {
				return ResponseEntity.ok(addSuccess(Globals.CODE_200, "", instruments));
			} else {
				error = addError(
						Globals.CODE_400,
						messageSource.getMessage("client.message.instrument.by.client.not.found", 
								new String[] {id.toString()}, 
								LocaleContextHolder.getLocale()) 
						);
			}
		} catch (Exception e) {
			log.error("ERROR ON FINDING INSTRUMENT BY CLIENT ID");
			log.error(e.getMessage());
			log.error(e.getCause().toString());
			error.setCause(e.getMessage() +" : "+ e.getCause().toString());			
		}		
		return ResponseEntity.badRequest().body(error);
	}
}
