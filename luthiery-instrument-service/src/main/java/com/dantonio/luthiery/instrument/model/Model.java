package com.dantonio.luthiery.instrument.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Model {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_model")
	private Long idModel;
	
	@NotNull(message="Instrument model name cannot be null")
	@Size(min=2, message="Instrument model name should have at least 2 characters")
	private String name;	
	
	@ManyToOne
	@JoinColumn(name="id_brand")
	@NotNull(message="Brand cannot be null")
	@Size(min=2, message="Brand should have at least 2 characters")
	@JsonIgnoreProperties("models")
	private Brand brand;
	
	@OneToMany
	@JoinColumn(name="id_model")
	@JsonIgnoreProperties("model")
	private Set<Instrument> instruments;

	public Long getIdModel() {
		return idModel;
	}

	public void setIdModel(Long idModel) {
		this.idModel = idModel;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public Set<Instrument> getInstruments() {
		return instruments;
	}

	public void setInstruments(Set<Instrument> instruments) {
		this.instruments = instruments;
	}
	
	
}
