package com.dantonio.luthiery.instrument.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.dantonio.luthiery.helper.bo.ClientBO;
import com.dantonio.luthiery.instrument.model.Instrument;

public interface InstrumentService {
	
	public Long createInstrument(Instrument instrument) throws Exception;
	
	public Collection<Instrument> findAllInstruments() throws Exception;
	
	public Optional<Instrument> findInstrumentById(Long id) throws Exception;	
	
	public Optional<ClientBO> getClient(Long idClient) throws Exception;
	
	public Collection<Instrument> findInstrumentsByTypeId(Long instrumentId) throws Exception;
	
	public List<Instrument> findInstrumentsByClientId(Long idClient) throws Exception;
}
