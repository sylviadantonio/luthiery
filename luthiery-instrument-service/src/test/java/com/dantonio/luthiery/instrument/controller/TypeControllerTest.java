package com.dantonio.luthiery.instrument.controller;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.dantonio.luthiery.helper.system.AbstractMessage;
import com.dantonio.luthiery.instrument.model.Type;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TypeControllerTest {
	
	@LocalServerPort
	private int port;
	
	TestRestTemplate restTemplate = new TestRestTemplate();
	
	HttpHeaders headers = new HttpHeaders();
	
	@Test
	public void testAddType() throws Exception {
		Type tipo = new Type("Test New Type");
		HttpEntity<Type> entity = new HttpEntity<>(tipo, headers);
		
		ResponseEntity<AbstractMessage> response = restTemplate.exchange (
				createURLWithPort("/add"), HttpMethod.POST, entity, AbstractMessage.class);
		
		String actual = response.getHeaders().get(HttpHeaders.LOCATION).get(0);
		
		assertTrue(actual.contains("/add"));
	}
	
	private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

}
