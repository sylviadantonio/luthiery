package com.dantonio.luthiery.instrument.mock;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.dantonio.luthiery.instrument.model.Type;
import com.dantonio.luthiery.instrument.model.repository.TypeRepository;
import com.dantonio.luthiery.instrument.service.TypeService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TypeControllerMockTest {
	
	@Autowired
	private TypeService typeService;
	
	@MockBean
	private TypeRepository typeRepository;
	
	@Test
	public void testRetrieveTypeWithMockRepository() throws Exception {
		
		Optional<Type> type = Optional.of( new Type("Test New Type"));
		when(typeRepository.findById(Long.valueOf(1))).thenReturn(type);
		
		assertTrue(typeService.findTypeById(Long.valueOf(1)).get().getName().contains("test"));
	}
	
	public void testAddTypeMockRepository(Type type) {
		
	}
	
}
