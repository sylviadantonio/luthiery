package com.dantonio.luthiery.luthierygatewayservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy
public class LuthieryGatewayServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LuthieryGatewayServiceApplication.class, args);
	}

}
