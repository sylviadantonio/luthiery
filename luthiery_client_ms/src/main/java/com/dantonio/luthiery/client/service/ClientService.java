package com.dantonio.luthiery.client.service;

import java.util.List;
import java.util.Optional;

import com.dantonio.luthiery.client.model.Client;
import com.dantonio.luthiery.helper.bo.InstrumentBO;

public interface ClientService {

	public Long createClient(Client client) throws Exception;
	
	public boolean updateClient(Client client) throws Exception;
	
	public boolean deleteClient(Long idClient) throws Exception;
	
	public List<Client> findAllClients() throws Exception;
	
	public Optional<Client> findClientById(Long idClient) throws Exception;
	
	public Optional<List<InstrumentBO>> findInstrumentsByClient(Long idClient) throws Exception;
	
	public Optional<Client> FindByEmail(String email) throws Exception;
}
