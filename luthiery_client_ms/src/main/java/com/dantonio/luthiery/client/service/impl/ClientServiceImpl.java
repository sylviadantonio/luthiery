package com.dantonio.luthiery.client.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.dantonio.luthiery.client.model.Client;
import com.dantonio.luthiery.client.model.repository.ClientRepository;
import com.dantonio.luthiery.client.service.ClientService;
import com.dantonio.luthiery.helper.bo.InstrumentBO;
import com.dantonio.luthiery.helper.system.AbstractMessage;
import com.dantonio.luthiery.helper.system.Globals;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;


@Service
public class ClientServiceImpl implements ClientService{

	private static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);
	
	@Autowired
	ClientRepository clientRepository;
	
	private String GET_INSTRUMENTS_BY_CLIENT_ADDRESS = "http://%1$s:%2$s/instrument/find-instrument-by-client-id/%3$s";
	
	@Value("${service.instrument.service}")
    private String instrumentServiceId;
	
	@Autowired
	private EurekaClient eurekaClient;
	
	@Autowired
    private RestTemplate restTemplate;
	
	@Override
	public Long createClient(Client client) throws Exception { 
		try {
			Client clientAdded = clientRepository.save(client);
			return clientAdded.getIdClient();
		} catch (Exception ex) {
			log.error("ERROR ON CREATING NEW CLIENT: " + ex.getMessage());
			log.error(ex.getCause().toString());
		}
		return Long.valueOf(0);
	}

	@Override
	public boolean updateClient(Client client) throws Exception {
		try {
			clientRepository.save(client);
			return true;
		} catch (Exception ex) {
			log.error("ERROR ON EDITING CLIENT " + client.getIdClient() +" : ---> " + ex.getMessage());
			log.error(ex.getCause().toString());
			throw new Exception();
		}
	}

	@Override
	public boolean deleteClient(Long idClient) throws Exception {
		try{
			clientRepository.deleteById(idClient);
			return true;
		} catch(Exception ex) {
			log.error("ERROR ON DELETING CLIENT " + idClient +" : ---> " + ex.getMessage());
			log.error(ex.getCause().toString());
			throw new Exception();
		}		
	}

	@Override
	public List<Client> findAllClients() throws Exception {
		try{
			return clientRepository.findAll();
		} catch(Exception ex) {
			log.error("ERROR ON RETURNING ALL CLIENTS: " + ex.getMessage());
			log.error(ex.getCause().toString());
			throw new Exception();
		}
	}

	@Override
	public Optional<Client> findClientById(Long idClient) throws Exception{
		try {
			return clientRepository.findById(idClient);			
		} catch(Exception ex) {
			log.error("ERROR ON FINDING CLIENT " + idClient +" : ---> " + ex.getMessage());
			log.error(ex.getCause().toString());
			throw new Exception();
		}
	}
	
	@Override
	public Optional<Client> FindByEmail(String email) throws Exception{
		return clientRepository.findByEmail(email);
	}
	
	@Override
	public Optional<List<InstrumentBO>> findInstrumentsByClient(Long idClient) throws Exception {
		try {
			Application eurekApp = eurekaClient.getApplication(instrumentServiceId);
			InstanceInfo instanceInfo = eurekApp.getInstances().get(0);
			
			String url = String.format(
					GET_INSTRUMENTS_BY_CLIENT_ADDRESS, 
					instanceInfo.getIPAddr(),
					instanceInfo.getPort(), 
					idClient);
						
			AbstractMessage result = restTemplate.getForObject(url, AbstractMessage.class);
			Optional<List<InstrumentBO>> instruments = Optional.ofNullable(null);
			if(result.getResult().equalsIgnoreCase(Globals.SUCCESS)) {
				instruments = Optional.of(
								new ObjectMapper().readValue(
										result.getObjReturn(), 
										new TypeReference<List<InstrumentBO>>(){}
									)
								);
			}
			return instruments;			
		} catch (Exception ex) {
			log.error("ERRO AO BUSCAR INSTRUMENTOS DE CLIENTE: " + ex.getClass().getName(), ex.getMessage());
			ex.printStackTrace();
			throw new Exception();			
		}
	}	
}