package com.dantonio.luthiery.client.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dantonio.luthiery.client.model.Client;
import com.dantonio.luthiery.client.service.ClientService;
import com.dantonio.luthiery.helper.bo.InstrumentBO;
import com.dantonio.luthiery.helper.controller.AbstractController;
import com.dantonio.luthiery.helper.system.AbstractMessage;
import com.dantonio.luthiery.helper.system.ErrorMessage;
import com.dantonio.luthiery.helper.system.Globals;



@Controller
@RequestMapping(path="/client")
public class ClientController extends AbstractController{

	private static final Logger log = LoggerFactory.getLogger(ClientController.class);
	
	@Autowired
	private ClientService clientService;
	@Autowired
	MessageSource messageSource;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@PostMapping( value="/add", consumes = {MediaType.APPLICATION_JSON_VALUE} )	
	public @ResponseBody ResponseEntity<AbstractMessage> addNewClient(@Valid @RequestBody Client client, BindingResult bindingResult) {
		ErrorMessage error = new ErrorMessage();
		try {
			if(bindingResult.hasErrors()) {
				error = addError(400, 
							messageSource.getMessage("standard.client.messages.insert.failed", 
							null, 
							LocaleContextHolder.getLocale()), 
							bindingResult);
			} else {
				client.setDate(new Date());		
				client.setPassword(bCryptPasswordEncoder.encode(client.getPassword()));
				if(clientService.createClient(client) != 0) {
					return ResponseEntity.ok(addSuccess(201, 
							messageSource.getMessage("standard.client.messages.insert.success", 
							new String[] {Client.class.getSimpleName()}, LocaleContextHolder.getLocale())));
				}				
			}
		} catch(Exception ex) {
			log.error("CLIENT INSERT FAILED: " + ex.getMessage());
			log.error(ex.getCause().toString());			
			error = addError(417,
					messageSource.getMessage("standard.messages.unexpected.error", null, LocaleContextHolder.getLocale()),
					ex.getMessage()
			);
		}
		return ResponseEntity.badRequest().body(error);
	}
	
	@GetMapping(path="/list-all")
	public @ResponseBody ResponseEntity<AbstractMessage> getAllClients() {
		ErrorMessage error = new ErrorMessage();
		try {			
			return ResponseEntity.ok(addSuccess(201, "", clientService.findAllClients()));			
		} catch (Exception e) {
			error = addError(417, 
					messageSource.getMessage("standard.client.message.notfound",  new String[] {Client.class.getSimpleName()}, LocaleContextHolder.getLocale()),
					e.getMessage()
					);
			log.error("FIND ALL CLIENTS FAILED: " + e.getMessage());
			log.error(e.getCause().toString());
		}
		return ResponseEntity.badRequest().body(error);
	}
	
	@GetMapping(path="/find/{id}")
	public @ResponseBody ResponseEntity<AbstractMessage> findClientById(@PathVariable Long id) {
		ErrorMessage error = new ErrorMessage();
		try{
			Optional<Client> client = clientService.findClientById(id);
			if(client.isPresent()) {
				return ResponseEntity.ok(addSuccess(201,"", client.get()));
			} else {
				error = addError(400,
						messageSource.getMessage("standard.client.message.notfound.by.id", new String[] {Client.class.getSimpleName(), id.toString()}, LocaleContextHolder.getLocale()));
			}
		} catch(Exception ex) {
			log.error("ERROR ON FINDING CLIENT BY ID "+ id + ": " + ex.getMessage());
			log.error(ex.getCause().toString());
			error.setCause(ex.getMessage() + ":" + ex.getCause().toString());			
		}
		return ResponseEntity.ok(error);
	}
	
	@PutMapping(path="/edit")
	public @ResponseBody ResponseEntity<AbstractMessage> updateClient(@Valid @RequestBody Client client, BindingResult bindingResult) {
		ErrorMessage error = new ErrorMessage();
		try {
			if(bindingResult.hasErrors()) {
				error = addError(400,
							messageSource.getMessage("standard.client.messages.update.failed", 
							null,
							LocaleContextHolder.getLocale()), 
							bindingResult);
			} else {
				client.setDate(new Date());				
				if(clientService.createClient(client) != 0) {
					return ResponseEntity.ok(addSuccess(201, 
							messageSource.getMessage("standard.client.messages.update.success", 
							new String[] {Client.class.getSimpleName()}, LocaleContextHolder.getLocale())));
				}
			}
		} catch(Exception ex) {
			log.error("CLIENT ID "+ client.getIdClient() +" UPDATE FAILED: " + ex.getMessage());
			log.error(ex.getCause().toString());			
			error = addError(417,
					messageSource.getMessage("standard.messages.unexpected.error", null, LocaleContextHolder.getLocale()),
					ex.getMessage()
			);
		}
		return ResponseEntity.badRequest().body(error);
	}
	
	@GetMapping(path="/get-instruments/{clientId}")
	public @ResponseBody ResponseEntity<AbstractMessage> getInstrumentsByClientId(@PathVariable Long clientId) {
		ErrorMessage error = new ErrorMessage();
		try {			
			Optional<List<InstrumentBO>> instruments = clientService.findInstrumentsByClient(clientId);
			return ResponseEntity.ok(addSuccess(201, "", instruments.get()));
		} catch (Exception e) {
			error = addError(417,
					messageSource.getMessage("standard.messages.unexpected.error", null, LocaleContextHolder.getLocale()),
					e.getMessage());
			log.error("ERROR ON GETTING INSTRUMENTS BY CLIENT ID: " + clientId);
			log.error(e.getMessage() +" : " + e.getCause().toString());			
		}
		
		return ResponseEntity.badRequest().body(error);
	}
	
	@DeleteMapping(path="/delete/{id}")
	public @ResponseBody ResponseEntity<AbstractMessage> deleteClient(@PathVariable Long idClient) {
		ErrorMessage error = new ErrorMessage();
		try {
			Optional<List<InstrumentBO>> instruments = clientService.findInstrumentsByClient(idClient);
			if(instruments.isPresent() && !instruments.get().isEmpty()) {
					error = addError(Globals.CODE_400, 
							messageSource.getMessage("client.message.delete.not.possible.client.has.instrument", null, LocaleContextHolder.getLocale()));
			} else  {
				//deleta usuario
				if(clientService.deleteClient(idClient)) {
					return ResponseEntity.ok(addSuccess(Globals.CODE_201,
							messageSource.getMessage("standard.client.messages.delete.success", 
									new String[] {Client.class.getSimpleName()}, 
									LocaleContextHolder.getLocale())));
				} else {
					error = addError(Globals.CODE_400, 
							messageSource.getMessage("standard.client.messages.delete.failed", null, LocaleContextHolder.getLocale()));
				}
			}
		} catch(Exception ex) {
			error = addError(417,
					messageSource.getMessage("standard.messages.unexpected.error", null, LocaleContextHolder.getLocale()),
					ex.getMessage());
			log.error("ERROR ON DELETING CLIENT ID: " + idClient);
			log.error(ex.getMessage() + " : " + ex.getCause().toString());
		}		
		return ResponseEntity.ok(error);
	}
}
