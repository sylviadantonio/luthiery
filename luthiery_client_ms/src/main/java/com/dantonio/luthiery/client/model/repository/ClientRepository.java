package com.dantonio.luthiery.client.model.repository;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dantonio.luthiery.client.model.Client;



public interface ClientRepository extends JpaRepository<Client, Long>{
	
	public Optional<Client> findByMobile(long id);

	public Optional<Client> findByEmail(String email);
}
