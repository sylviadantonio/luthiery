package com.dantonio.luthiery.client.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.JWT;
import com.dantonio.luthiery.client.model.Client;
import com.dantonio.luthiery.helper.security.SecurityConstants;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter{

	@Autowired
	private AuthenticationManager autenticationManager;
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException{
		
		try {
			Client client  = new ObjectMapper().readValue(request.getInputStream(), Client.class);
			
			return autenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
								client.getEmail(), client.getPassword(), new ArrayList<>()));
			
		} catch(IOException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, 
												HttpServletResponse response, 
												FilterChain filter, 
												Authentication auth) throws IOException, ServletException{
		String token = JWT.create()
						.withSubject(((User) auth.getPrincipal()).getUsername())
						.withExpiresAt(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
						.sign(Algorithm.HMAC512(SecurityConstants.SECRET.getBytes()));
		
		response.addHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + token);
	}
	
}
