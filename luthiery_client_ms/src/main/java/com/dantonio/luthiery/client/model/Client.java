package com.dantonio.luthiery.client.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Client {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_client")
	private Long idClient;
	
	@NotNull(message="Client name cannot be null.")
	@Size(min=2, message="Client name should have at least 2 characters.")
	private String name;
	
	@NotNull(message="Client email cannot be null.")
	@Size(min=2, message="Client email should have at least 2 characters.")
	@Email
	private String email;
	
	@NotNull(message="Client password cannot be null.")
	@Size(min=8, message="Client passowrd should have at least 8 characters.")
	private String password;
	
	
	@NotNull(message="Client mobile cannot be null.")
	@Size(min=8, message="Client mobile should have at least 8 characters.")
	private String mobile;
	
	private String phone;
	private Date date;
	private String referral;
	
	public Long getIdClient() {
		return idClient;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getReferral() {
		return referral;
	}

	public void setReferral(String referral) {
		this.referral = referral;
	}
	
}
