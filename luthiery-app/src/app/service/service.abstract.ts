import { HttpClient, HttpHeaders } from '@angular/common/http';

export class ServiceAbstract {
    constructor(httpClient: HttpClient) { }
    
    public firstPage: string = "";
    public prevPage: string = "";
    public nextPage: string = "";
    public lastPage: string = "";

    parse_link_header(header) {
        if (header == null || header.length == 0) {
          return ;
        }
    
        let parts = header.split(',');
        var links = {};
        parts.forEach( p => {
          let section = p.split(';');
          var url = section[0].replace(/<(.*)>/, '$1').trim();
          var name = section[1].replace(/rel="(.*)"/, '$1').trim();
          links[name] = url;
    
        }); 
        return links;
    }

    public retrieve_pagination_links(response){
        const linkHeader = this.parse_link_header(response.headers.get('Link'));
        this.firstPage = linkHeader["first"];
        this.lastPage =  linkHeader["last"];
        this.prevPage =  linkHeader["prev"];
        this.nextPage =  linkHeader["next"];
    }

    public getHeader(contentType) {
      return new HttpHeaders().set('Content-Type', contentType)
    }
}