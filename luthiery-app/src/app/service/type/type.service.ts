import { Injectable } from '@angular/core';
import { ServiceAbstract } from '../service.abstract';
import { Globals } from 'src/app/utils/Globals';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Type } from 'src/app/model/type/type';

@Injectable({
  providedIn: 'root'
})
export class TypeService extends ServiceAbstract{

  apiURL: string = `${Globals.SERVICE_INSTRUMENT_API}${Globals.TYPE_URL}`;
  constructor(private httpClient: HttpClient) {
    super(httpClient);    
  }

  public createType(type: Type) {
    this.httpClient
      .post<Type>(`${this.apiURL}${Globals.CREATE_URL}`,
        JSON.stringify(type),{
          headers: this.getHeader(Globals.CONTENT_TYPE_JSON)
        }) 
      .subscribe(
        data => {
          console.log(data) 
        },
        error => console.log("error: ", error)
      )

  }

  public updateType(type: Type) {}

  public deleteType(id_type: number) {}

  public getTypeById(id_type: number){}

  public getTypes(url? : string) {
    if(url) {
      return this.httpClient    
      .get<Type[]>(`${this.apiURL}${Globals.LIST_ALL_URL}`,
        { observe: 'response'})
      .pipe( 
          tap (res => {
        this.retrieve_pagination_links(res)
      }))
    }
    
    return this.httpClient    
      .get<Type[]>(`${this.apiURL}${Globals.LIST_ALL_URL}`,
      { observe: 'response'})
  }

  public getTypesComplete(url? : string) {
    if(url) {
      return this.httpClient    
      .get<Type[]>(`${this.apiURL}${Globals.LIST_ALL_URL}`,
        { observe: 'response'})
      .pipe( 
          tap (res => {            
              this.retrieve_pagination_links(res)
      }))
    }
    return this.httpClient    
      .get<Type[]>(`${this.apiURL}${Globals.LIST_ALL_COMPLETE_URL}`,
        { observe: 'response'})
  }  
}
