export class Globals {
    //services address
    public static SERVICE_INSTRUMENT_ADDRESS : string = "http://localhost:8083/";
    public static SERVICE_CLIENT_ADDRESS : string= "http://localhost:8082/";

    public static SERVICE_INSTRUMENT_API : string = "/api-instrument/";
    public static SERVICE_CLIENT_API : string = "/api-client/";

    //standard operations
    public static CREATE_URL = "/add";    
    public static UPDATE_URL = "/edit/";    
    public static DELETE_URL = "/delete";
    public static FIND_BY_ID_URL = "/find/";
    public static LIST_ALL_URL = "/list-all";
    public static LIST_ALL_COMPLETE_URL = "/list-all-complete";
    
    public static CONTENT_TYPE_JSON = "application/json";
    //services
    public static TYPE_URL = "type";

}