import { Component, OnInit } from '@angular/core';
import { FormGroupName, FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { TypeService } from 'src/app/service/type/type.service';

@Component({
  selector: 'app-instrument',
  templateUrl: './instrument.component.html',
  styleUrls: ['./instrument.component.css']
})
export class InstrumentComponent implements OnInit {

  instrumentForm : FormGroup
  instrumentInfo : FormGroupName
  
  constructor(private fb : FormBuilder,
    private typeService : TypeService) { }

  ngOnInit() {
    this.instrumentForm =  new FormGroup({
      instrumentInfo : this.fb.group({
        instrument : this.fb.array([
          this.initInstrument()
        ])
      })
    })
    console.log(this.instrumentForm)
    this.typeService.getTypes().subscribe(res => {
      console.log(res)
    });
  }

  initInstrument() {
    return this.fb.group({
      color : ['', Validators.required]
    })
  }

  addInstrument() {
    const control = this.instrumentForm.controls.instrumentInfo.get('instrument') as FormArray;
    control.push(this.initInstrument())

    console.log(this.instrumentForm)
  }

  removeInstrument(i : number) {
    const control = this.instrumentForm.controls.instrumentInfo.get('instrument') as FormArray;
    control.removeAt(i)
  }
}
