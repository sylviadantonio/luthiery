import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeGridComponent } from './type-grid.component';

describe('TypeGridComponent', () => {
  let component: TypeGridComponent;
  let fixture: ComponentFixture<TypeGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
