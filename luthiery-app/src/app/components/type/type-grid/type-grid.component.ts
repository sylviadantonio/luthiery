import { Component, OnInit } from '@angular/core';
import { TypeService } from 'src/app/service/type/type.service';
import { TypeEditorComponent } from '../type-editor/type-editor.component';


@Component({
  selector: 'type-grid',
  templateUrl: './type-grid.component.html',
  styleUrls: ['./type-grid.component.css']
})
export class TypeGridComponent implements OnInit {

  columnDefs = [
    {
      headerName : 'ID', 
      field: 'idType', 
      sortable: true, 
      filter: true
    },
    {
      headerName : 'NAME', 
      field: 'name', 
      sortable: true, 
      filter: true, 
      editable: true,
      cellEditor: TypeEditorComponent,
    },
  ]

   
  rowData = [];

  constructor(private typeService: TypeService) {
  }

  ngOnInit() {
    
    this.typeService.getTypes().subscribe(response => {
      this.editListObj(response.body)
    })
  }

  editListObj(list) {
    this.rowData = JSON.parse(list.objReturn)
    this.rowData.forEach(item => {
      delete item.instruments
    });
    //console.log(this.rowData)
  }
  
  
}
