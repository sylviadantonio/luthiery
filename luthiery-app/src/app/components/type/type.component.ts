import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { TypeService } from 'src/app/service/type/type.service';

@Component({
  selector: 'app-type',
  templateUrl: './type.component.html',
  styleUrls: ['./type.component.css']
})
export class TypeComponent implements OnInit {

  typeForm : FormGroup
  isSubmitted = false

  constructor(private fb : FormBuilder, 
    private typeService: TypeService) { }

  ngOnInit() {
    this.typeForm = this.fb.group({
      name : ['', Validators.required]
    })

    this.typeService.getTypes().subscribe(res => {
      console.log(res)
    })
  }

  onSubmit() {
    this.isSubmitted = true;
    // stop the process here if form is invalid
    if (this.typeForm.invalid) {
        return;
    }

    console.log(this.typeForm)
    this.typeService.createType(this.typeForm.controls.name.value)
  }
}
