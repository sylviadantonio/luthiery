import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-type-editor',
  templateUrl: './type-editor.component.html',
  styleUrls: ['./type-editor.component.css']
})
export class TypeEditorComponent implements AfterViewInit{

  @ViewChild('i', {static: false}) form
  params

  agInit(params: any): void {
    this.params = params
  }

  ngAfterViewInit() {
    setTimeout(() => {
      //this.textInput.nativeElement.focus()
    })
  }

  getValue() {
    //return this.textInput.nativeElement.value
    console.log(this.form)
  }  

  onSubmit(event) {
    console.log(event)
  }

}
