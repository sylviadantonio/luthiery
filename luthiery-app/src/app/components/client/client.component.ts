import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupName, FormArray } from '@angular/forms';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})

export class ClientComponent implements OnInit {
  
  clientForm : FormGroup
  clientInfo : FormGroupName  

  constructor(private fb : FormBuilder){ }

  ngOnInit() {
    this.clientForm = new FormGroup({
      clientInfo: this.fb.group({
        name : ['', Validators.required],
        email : ['', Validators.required, Validators.email],
        mobile : ['', Validators.required],
        phone : [''],
        referral : ['']
      }),
      
    }) 
    console.log(this.clientForm)
  }
  
  onSubmit(formElement) {
    console.log(formElement.value)
  }
}
