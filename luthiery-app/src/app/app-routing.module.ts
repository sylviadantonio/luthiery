import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { ClientComponent } from './components/client/client.component';
import { InstrumentComponent } from './components/instrument/instrument.component';
import { TypeComponent } from './components/type/type.component';

 
const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'client', component:ClientComponent },
  { path: 'instrument', component:InstrumentComponent },
  { path: 'type', component:TypeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
