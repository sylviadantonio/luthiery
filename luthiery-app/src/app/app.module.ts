import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ClientComponent } from './components/client/client.component';
import { InstrumentComponent } from './components/instrument/instrument.component';
import { HttpClientModule } from '@angular/common/http';
import { TypeComponent } from './components/type/type.component';
import { TypeGridComponent } from './components/type/type-grid/type-grid.component';
import { AgGridModule } from 'ag-grid-angular';
import { TypeEditorComponent } from './components/type/type-editor/type-editor.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ClientComponent,
    InstrumentComponent,
    TypeComponent,
    TypeGridComponent,
    TypeEditorComponent,        
  ],
  imports: [     
    AppRoutingModule,
    ReactiveFormsModule,
    BsDropdownModule,
    TooltipModule,
    ModalModule,
    BrowserModule,
    HttpClientModule,
    AgGridModule.withComponents([
      TypeEditorComponent
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
