package com.dantonio.luthiery.helper.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Util {
	
	public static Optional<String> convertListToJson(List list) {
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
	    final ObjectMapper mapper = new ObjectMapper();
	    Optional<String> result = Optional.of(new String(""));
	    try {
			mapper.writeValue(out, list);
			final byte[] data = out.toByteArray();
		    result = Optional.of(new String(data));
		} catch (IOException e) {
			e.printStackTrace();
		}	    
	    return result;
	}
}
