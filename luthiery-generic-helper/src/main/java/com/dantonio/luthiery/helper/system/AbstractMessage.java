package com.dantonio.luthiery.helper.system;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AbstractMessage {
	
	
	public AbstractMessage() {
		
	}
	
	AbstractMessage(String result, int code, String message, String objReturn) {
		this.code = code;
		this.message = message;
		this.objReturn = objReturn;		
	}
	
	@JsonProperty("result")
	private String result;
	@JsonProperty("code")
	private int code;
	@JsonProperty("message")
	private String message;
	@JsonProperty("objReturn")
	private String objReturn;
	
	
	public void setResult(String result) {
		this.result = result;
	}
	
	public String getResult() { 
		return result;
	}

	public int getCode() {
	    return code;
	}

	public void setCode(int code) {
	    this.code = code;
	}

	public String getMessage() {
	    return message;
	}

	public void setMessage(String message) {
	    this.message = message;
	}	
	
	public void setObjReturn(String objReturn) {
		this.objReturn = objReturn;
	}
	
	public String getObjReturn() {
		return this.objReturn;
	}
	
}
