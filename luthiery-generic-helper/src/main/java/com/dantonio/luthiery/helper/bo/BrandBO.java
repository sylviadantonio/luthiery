package com.dantonio.luthiery.helper.bo;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BrandBO {

	@JsonProperty("idBrand")
	private Long idBrand;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("models")
	private Set<ModelBO> models;

	public Long getIdBrand() {
		return idBrand;
	}

	public void setIdBrand(Long idBrand) {
		this.idBrand = idBrand;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<ModelBO> getModels() {
		return models;
	}

	public void setModels(Set<ModelBO> models) {
		this.models = models;
	}	
	
}
