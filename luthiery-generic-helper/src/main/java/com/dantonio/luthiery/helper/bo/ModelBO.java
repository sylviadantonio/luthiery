package com.dantonio.luthiery.helper.bo;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ModelBO {

	@JsonProperty("idModel")
	private Long idModel;
	
	@JsonProperty("name")
	private String name;	
	
	@JsonProperty("brand")
	private BrandBO brand;
	
	@JsonProperty("instruments")
	private Set<InstrumentBO> instruments;

	public Long getIdModel() {
		return idModel;
	}

	public void setIdModel(Long idModel) {
		this.idModel = idModel;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BrandBO getBrand() {
		return brand;
	}

	public void setBrand(BrandBO brand) {
		this.brand = brand;
	}

	public Set<InstrumentBO> getInstruments() {
		return instruments;
	}

	public void setInstruments(Set<InstrumentBO> instruments) {
		this.instruments = instruments;
	}
}
