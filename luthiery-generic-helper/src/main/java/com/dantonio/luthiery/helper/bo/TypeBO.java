package com.dantonio.luthiery.helper.bo;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TypeBO {

	@JsonProperty("idType")
	private Long idType;

	@JsonProperty("name")
	private String name;
	
	@JsonProperty("instruments")
	private Set<InstrumentBO> instruments;

	public TypeBO() { }
	
	public TypeBO(String name) {
		this.name = name;
	}
	
	public Long getIdType() {
		return idType;
	}

	public void setIdType(Long idType) {
		this.idType = idType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<InstrumentBO> getInstruments() {
		return instruments;
	}

	public void setInstruments(Set<InstrumentBO> instruments) {
		this.instruments = instruments;
	}

	
}
