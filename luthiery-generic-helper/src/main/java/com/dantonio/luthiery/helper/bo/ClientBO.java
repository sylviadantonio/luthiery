package com.dantonio.luthiery.helper.bo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientBO{

	@JsonProperty("idClient")
	Long idClient;	

	@JsonProperty("name")
	String name;
	
	@JsonProperty("email")
	String email;
	
	@JsonProperty("mobile")
	String mobile;
	
	@JsonProperty("phone")
	String phone;
	
	@JsonProperty("date")
	Date date;
	
	@JsonProperty("referral")
	String referral;
	
	public Long getIdClient() {
		return idClient;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getReferral() {
		return referral;
	}

	public void setReferral(String referral) {
		this.referral = referral;
	}
	
	public String toString() {
		return "id: " +this.idClient + "\n" +
				"name: " + this.name + "\n" +
				"email: " + this.mobile + "\n" +
				"phone: " + this.phone + "\n" +
				"date: " + this.date + "\n" +
				"referral: " + this.referral;				
	}
	
}
