package com.dantonio.luthiery.helper.bo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InstrumentBO {

	@JsonProperty("idInstrumento")
	private Long idInstrumento;
	
	@JsonProperty("color")
	private String color;
	
	@JsonProperty("model")
	private ModelBO model;
	
	@JsonProperty("type")
	private TypeBO type;
	
	@JsonProperty("client")
	private ClientBO client;
	
	@JsonProperty("numberOfStrings")
	private String numberOfStrings;
	
	@JsonProperty("idClient")
	private Long idClient;
	
	@JsonProperty("uuid")
	private String uuid;
	
	@JsonProperty("dateCreation")
	private Date dateCreation;
	
	public Long getIdInstrumento() {
		return idInstrumento;
	}
	public void setIdInstrumento(Long idInstrumento) {
		this.idInstrumento = idInstrumento;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public ModelBO getModel() {
		return model;
	}
	public void setModel(ModelBO model) {
		this.model = model;
	}	
	public ClientBO getClient() {
		return client;
	}
	public void setClient(ClientBO client) {
		this.client = client;
	}
	public TypeBO getType() {
		return type;
	}
	public void setType(TypeBO type) {
		this.type = type;
	}
	
	public String getNumberOfStrings() {
		return numberOfStrings;
	}
	public void setNumberOfStrings(String numberOfStrings) {
		this.numberOfStrings = numberOfStrings;
	}
	public Long getIdClient() {
		return idClient;
	}
	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
}
