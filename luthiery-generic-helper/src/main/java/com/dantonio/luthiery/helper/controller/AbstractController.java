package com.dantonio.luthiery.helper.controller;

import org.springframework.validation.BindingResult;

import com.dantonio.luthiery.helper.system.ErrorMessage;
import com.dantonio.luthiery.helper.system.SuccessMessage;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AbstractController {

	protected ErrorMessage addError(int code, String msg, BindingResult bindingResult) {
		ErrorMessage error = new ErrorMessage();
		error.setCode(code);	
		error.setMessage(msg);
		error.setCause(bindingResult);
		
		return error;
	}
	
	protected ErrorMessage addError(int code, String msg, String cause) {
		ErrorMessage error = new ErrorMessage();
		error.setCode(code);	
		error.setMessage(msg);
		error.setCause(cause);
		
		return error;
	}
	protected ErrorMessage addError(int code, String msg) {
		ErrorMessage error = new ErrorMessage();
		error.setCode(code);	
		error.setMessage(msg);		
		
		return error;
	}
	
	protected SuccessMessage addSuccess(int code, String message) {
		SuccessMessage success = new SuccessMessage();
		success.setCode(code);
		success.setMessage(message);
		return success;
	}
	
	@SuppressWarnings("finally")
	protected SuccessMessage addSuccess(int code, String message, Object object) {
		SuccessMessage success = new SuccessMessage();
		success.setCode(code);
		success.setMessage(message);
		try {			 
			ObjectMapper mapper = new ObjectMapper();			
			success.setObjReturn(mapper.writeValueAsString(object));			
		} catch (Exception ex) {
			System.out.println("Error on converting object to string.");
			System.out.println(ex.getClass() + " " + ex.getMessage());
			ex.printStackTrace();
			success.setMessage("THERE WAS AN ERROR TO CONVERT OBJECT TO STRING");
		} finally {
			return success;
		}
	}
	
}
