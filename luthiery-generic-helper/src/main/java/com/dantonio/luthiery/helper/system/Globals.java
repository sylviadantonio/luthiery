package com.dantonio.luthiery.helper.system;

public class Globals {

	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	public static final int CODE_200 = 200;
	public static final int CODE_201 = 201;
	public static final int CODE_400 = 400;
	public static final int CODE_401 = 401;
}
