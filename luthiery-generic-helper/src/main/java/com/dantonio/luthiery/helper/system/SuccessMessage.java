package com.dantonio.luthiery.helper.system;

public class SuccessMessage extends AbstractMessage{
	
	private String objReturn;	
	
	public SuccessMessage() {
		this.setResult("success");		
	}
	
	@Override
	public String getObjReturn() {
		return objReturn;
	}
	
	@Override
	public void setObjReturn(String objReturn) {		
		this.objReturn = objReturn;
	}
	
	
}
