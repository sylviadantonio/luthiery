package com.dantonio.luthiery.helper.system;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.BindingResult;

public class ErrorMessage extends AbstractMessage{
	
	private String cause;
	
	public ErrorMessage() {
		this.setResult("error");
	}
	
	public String getCause() {
	    return cause;
	}

	public void setCause(String cause) {
	    this.cause = cause;
	}
	
	public void setCause(List<String> errors) {
		this.cause = errors.toString();
	}
	
	public void setCause(BindingResult bindingResult) {
		List<String> msgs = new ArrayList<>();		
		bindingResult.getFieldErrors().stream()
			.forEach(e-> {				
				msgs.add(e.getDefaultMessage());					
				});		
	    setCause(msgs);
	}

	@Override
	public void setObjReturn(String objReturn) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getObjReturn() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
